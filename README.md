## Class Based Components
TUrning functional components into class based.

---

## Lifecycle Methods:

- **componentDidMount** - runs once
- **componentDidUpdate** - runs everytime a component re-renders
- **componentWillUnmount** - runs just before the component will be unmounted
- **componentDidCatch** - makes the class based component an ==Error Boundary==. N\A for functional components.