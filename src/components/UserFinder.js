import { Fragment, useState, useEffect, Component } from 'react';

import Users from './Users';
import classes from './UserFinder.module.css';
import UsersContext from '../store/users-context';
import ErrorBoundary from './ErrorBoundary';

// const DUMMY_USERS = [
//     { id: 'u1', name: 'Max' },
//     { id: 'u2', name: 'Manuel' },
//     { id: 'u3', name: 'Julie' },
// ];

class UserFinder extends Component {
    static contextType = UsersContext;

    constructor() {
        super();
        this.state = {
            filteredUsers: [],
            searchTerm: ''
        };
    }
    // Runs once, when the component was initially rendered for the first time
    componentDidMount() {
        // this.setState({ filteredUsers: DUMMY_USERS });
        this.setState({ filteredUsers: this.context.users });
    }

    // Runs multiple times when component re-renders
    componentDidUpdate(prevProps, prevState) {
        if (prevState.searchTerm !== this.state.searchTerm) {
            this.setState({
                // filteredUsers: DUMMY_USERS.filter((user) => user.name.toLowerCase().includes(this.state.searchTerm.toLowerCase()))
                filteredUsers: this.context.users.filter((user) => user.name.toLowerCase().includes(this.state.searchTerm.toLowerCase()))
            });
        }
    }

    searchChangeHandler(event) {
        this.setState({ searchTerm: event.target.value });
    }

    render() {
        return (
            <Fragment>
                <div className={classes.finder}>
                    <input
                        type='search'
                        onChange={this.searchChangeHandler.bind(this)}
                    />
                </div>
                <ErrorBoundary>
                    <Users users={this.state.filteredUsers} />
                </ErrorBoundary>
            </Fragment>
        );
    }
}

// const UserFinder = () => {
//     const [filteredUsers, setFilteredUsers] = useState(DUMMY_USERS);
//     const [searchTerm, setSearchTerm] = useState('');

//     useEffect(() => {
//         setFilteredUsers(
//             DUMMY_USERS.filter((user) => user.name.toLowerCase().includes(searchTerm.toLowerCase()))
//         );
//     }, [searchTerm]);

//     const searchChangeHandler = (event) => {
//         setSearchTerm(event.target.value);
//     };

//     return (
//         <Fragment>
//             <div className={classes.finder}>
//                 <input type='search' onChange={searchChangeHandler} />
//             </div>
//             <Users users={filteredUsers} />
//         </Fragment>
//     );
// };

export default UserFinder;








// class UserFinder extends Component {
//     constructor() {
//         super();
//         this.state = {
//             filteredUsers: [],
//             searchTerm: '',
//         };
//     }

//     componentDidMount() {
//         // Send http request...
//         this.setState({ filteredUsers: DUMMY_USERS });
//     }

//     componentDidUpdate(prevProps, prevState) {
//         if (prevState.searchTerm !== this.state.searchTerm) {
//             this.setState({
//                 filteredUsers: DUMMY_USERS.filter((user) =>
//                     user.name.toLowerCase().includes(this.state.searchTerm.toLowerCase())
//                 ),
//             });
//         }
//     }

//     searchChangeHandler(event) {
//         this.setState({ searchTerm: event.target.value });
//     }

//     render() {
//         return (
//             <Fragment>
//                 <div className={classes.finder}>
//                     <input type='search' onChange={this.searchChangeHandler.bind(this)} />
//                 </div>
//                 <Users users={this.state.filteredUsers} />
//             </Fragment>
//         );
//     }
// }